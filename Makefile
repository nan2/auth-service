build: docker-build up dependencies

compose := docker-compose -f ./.docker/docker-compose.yml --env=./.docker/.env

docker-build:
	$(compose) build

up:
	$(compose) up -d

down:
	$(compose) down

dependencies:
	$(compose) exec -T fpm chmod a+w -R public/storage
	$(compose) exec -T fpm composer install
	$(compose) exec -T fpm php bin/console messenger:consume
