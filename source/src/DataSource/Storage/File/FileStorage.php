<?php declare(strict_types=1);

namespace App\DataSource\Storage\File;

use App\DataSource\Entity\File\FileEntityInterface;
use GlobIterator;

class FileStorage implements FileStorageInterface
{
    /**
     * @inheritdoc
     */
    public function write(string $path, string $data): void
    {
        file_put_contents($path, $data, LOCK_EX);
    }

    /**
     * @inheritdoc
     */
    public function read(string $path): string
    {
        if (!$this->exists($path)) {
            throw new \RuntimeException(sprintf('File "%s" not exists', $path));
        }

        return file_get_contents($path);
    }

    /**
     * @inheritdoc
     */
    public function remove(string $path): void
    {
        if (!$this->exists($path)) {
            throw new \RuntimeException(sprintf('File "%s" not exists', $path));
        }

        unlink($path);
    }

    /**
     * @inheritdoc
     */
    public function exists(string $path): bool
    {
        return file_exists($path);
    }

    /**
     * @inheritdoc
     */
    public function findFileByPattern(string $path, string $pattern): array
    {
        $result = [];
        $iterator = new GlobIterator($path.'/'.$pattern);

        while ($iterator->valid()) {
            $result[] = $iterator->current()->getFilename();
            $iterator->next();
        }

        return $result;
    }
}
