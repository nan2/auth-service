<?php declare(strict_types=1);

namespace App\DataSource\Storage\File;

use App\DataSource\Entity\File\FileEntityInterface;
use RuntimeException;

interface FileStorageInterface
{
    /**
     * Insert entity to file
     *
     * @param string $path
     * @param        $data
     */
    public function write(string $path, string $data): void;

    /**
     * @param string $path
     *
     * @return bool
     */
    public function exists(string $path): bool;

    /**
     * @param string $path
     *
     * @return mixed
     * @throws RuntimeException
     */
    public function read(string $path): string;

    /**
     * @param string $path
     *
     * @throws RuntimeException
     */
    public function remove(string $path): void;

    /**
     * @param string $path
     * @param string $pattern
     *
     * @return array
     */
    public function findFileByPattern(string $path, string $pattern): array;
}
