<?php

namespace App\DataSource\ValueObject;

use App\DataSource\Types\Enum\StringEnumInterface;
use App\DataSource\Types\Enum\StringEnumTrait;

class TrackingSourceTypeEnum implements StringEnumInterface
{
    use StringEnumTrait;

    public const HOME_PAGE = 'home_page';
    public const ABOUT_PAGE = 'about_page';
    public const CONTACT_PAGE = 'contact_page';
    public const SUBMIT_FEEDBACK_PAGE = 'submit_feedback_page';
}