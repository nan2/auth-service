<?php declare(strict_types=1);

namespace App\Domain\TrackingAction;

use App\Infrastructure\ServiceClient\AnalyticsServiceClient;
use DateTimeInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class TrackingActionManager
{
    /**
     * @var AnalyticsServiceClient
     */
    private AnalyticsServiceClient $analyticsServiceClient;

    /**
     * TrackingActionManager constructor.
     *
     * @param AnalyticsServiceClient $analyticsServiceClient
     */
    public function __construct(AnalyticsServiceClient $analyticsServiceClient)
    {
        $this->analyticsServiceClient = $analyticsServiceClient;
    }

    /**
     * @param string            $userId
     * @param string            $sourceType
     * @param DateTimeInterface $dateCreated
     *
     * @throws TransportExceptionInterface
     */
    public function trackAction(
        string $userId,
        string $sourceType,
        DateTimeInterface $dateCreated
    ): void {
        $this->analyticsServiceClient->trackAction($userId, $sourceType,
            $dateCreated);
    }
}
