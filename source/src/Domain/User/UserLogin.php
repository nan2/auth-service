<?php declare(strict_types=1);

namespace App\Domain\User;

use App\DataSource\Repository\UserRepository;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class UserLogin
{
    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    /**
     * @var TokenGenerator
     */
    private TokenGenerator $tokenGenerator;

    /**
     * UserLogin constructor.
     *
     * @param UserRepository $userRepository
     * @param TokenGenerator $tokenGenerator
     */
    public function __construct(
        UserRepository $userRepository,
        TokenGenerator $tokenGenerator
    ) {
        $this->userRepository = $userRepository;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @param string $nickName
     * @param string $password
     *
     * @return string
     */
    public function login(string $nickName, string $password): string
    {
        $user = $this->userRepository->getByNickName($nickName);

        if ($user->getPassword() === md5($password)) {
            $this->userRepository->updateToken($user,
                $this->tokenGenerator->generateToken());

            return $user->getToken();
        }

        throw new AuthenticationException();
    }
}