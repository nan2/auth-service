<?php declare(strict_types=1);

namespace App\Domain\User\Exception;

use Exception;
use Throwable;

class UserNickNameNotUniqueException extends Exception
{
    public function __construct(
        $message = "User login not unique",
        $code = 422,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
