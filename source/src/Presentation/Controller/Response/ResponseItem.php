<?php declare(strict_types=1);

namespace App\Presentation\Controller\Response;

use NaN\ApiBundle\Response\ApiJsonResponseInterface;

class ResponseItem implements ApiJsonResponseInterface
{
    /**
     * @var mixed
     */
    protected $data;

    /**
     * ResponseItem constructor.
     *
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }


}