<?php declare(strict_types=1);

namespace App\Presentation\Controller\Traits;

use App\Presentation\Controller\Response\ResponseItem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

trait ResponseControllerTrait
{
    private NormalizerInterface $responseNormalizer;

    /**
     * @required
     *
     * @param NormalizerInterface $normalizer
     */
    public function setNormalizer(NormalizerInterface $normalizer): void
    {
        $this->responseNormalizer = $normalizer;
    }

    /**
     * @param ResponseItem $data
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function buildResponse(ResponseItem $data): JsonResponse
    {
        return new JsonResponse($this->responseNormalizer->normalize($data));
    }
}