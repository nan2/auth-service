<?php declare(strict_types=1);

namespace App\Presentation\Controller\Request;

use NaN\ApiBundle\Request\PostRequestInterface;
use Symfony\Component\Validator\Constraints as Assert;

class UserRegister implements PostRequestInterface
{
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 2,
     *      max = 50
     * )
     */
    private string $firstName;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 2,
     *      max = 50
     * )
     */
    private string $lastName;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 5,
     *      max = 50
     * )
     */
    private string $nickName;

    /**
     * @var int
     * @Assert\Range(
     *      min = 10,
     *      max = 150
     * )
     */
    private int $age;


    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 5,
     *      max = 50
     * )
     */
    private string $password;

    /**
     * @var string|null
     * @Assert\Length(
     *      min = 5,
     *      max = 50
     * )
     */
    private ?string $guestToken = null;

    /**
     * Registration constructor.
     *
     * @param string      $firstName
     * @param string      $lastName
     * @param string      $nickName
     * @param int         $age
     * @param string      $password
     * @param string|null $guestToken
     */
    public function __construct(
        string $firstName,
        string $lastName,
        string $nickName,
        int $age,
        string $password,
        ?string $guestToken
    ) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->nickName = $nickName;
        $this->age = $age;
        $this->password = $password;
        $this->guestToken = $guestToken;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getNickName(): string
    {
        return $this->nickName;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string|null
     */
    public function getGuestToken(): ?string
    {
        return $this->guestToken;
    }
}
