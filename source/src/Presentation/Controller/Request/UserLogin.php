<?php declare(strict_types=1);

namespace App\Presentation\Controller\Request;

use NaN\ApiBundle\Request\PostRequestInterface;
use Symfony\Component\Validator\Constraints as Assert;


class UserLogin implements PostRequestInterface
{
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 5,
     *      max = 50
     * )
     */
    private string $nickName;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 5,
     *      max = 50
     * )
     */
    private string $password;

    public function __construct(string $nickName, string $password)
    {
        $this->nickName = $nickName;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getNickName(): string
    {
        return $this->nickName;
    }


    public function getPassword(): string
    {
        return $this->password;
    }
}