<?php declare(strict_types=1);

namespace App\Infrastructure\ServiceClient;

use DateTimeInterface;
use SocialTech\StorageInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AnalyticsServiceClient
{
    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $httpClient;

    /**
     * @var string
     */
    private string $host;

    /**
     * @var StorageInterface
     */
    private StorageInterface $storage;

    /**
     * @var string
     */
    private string $slowStoragePath;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * AnalyticsServiceClient constructor.
     *
     * @param HttpClientInterface $httpClient
     * @param string              $analyticsHost
     * @param StorageInterface    $storage
     * @param string              $slowStoragePath
     * @param SerializerInterface $serializer
     */
    public function __construct(
        HttpClientInterface $httpClient,
        string $analyticsHost,
        StorageInterface $storage,
        string $slowStoragePath,
        SerializerInterface $serializer
    ) {
        $this->httpClient = $httpClient;
        $this->host = $analyticsHost;
        $this->storage = $storage;
        $this->slowStoragePath = $slowStoragePath;
        $this->serializer = $serializer;
    }

    /**
     * @param string            $userId
     * @param string            $sourceType
     * @param DateTimeInterface $dateCreated
     *
     * @throws TransportExceptionInterface
     */
    public function trackAction(
        string $userId,
        string $sourceType,
        DateTimeInterface $dateCreated
    ) {
        //TODO: По логике здесь должен отправляться реквест к сервису Analytics, но вместо этого мы используем slow-storage-emulator

        /**
         * $url = "/track-action";
         *
         * $this->httpClient->request(Request::METHOD_POST, $this->makeUrl($url), [
         * 'json' => [
         * 'id_user' => $userId,
         * 'source_label' => $sourceType,
         * 'date_created' => $dateCreated->format('Y-m-d H:i:s')
         * ]
         * ]);*/

        $data = [
            'id_user'      => $userId,
            'source_label' => $sourceType,
            'date_created' => $dateCreated->format('Y-m-d H:i:s'),
        ];

        $this->storage->append($this->slowStoragePath,
            $this->serializer->serialize($data, 'json'));
    }

    /**
     * @param string $url
     *
     * @return string
     */
    private function makeUrl(string $url): string
    {
        return sprintf('%s%s', $this->host, $url);
    }
}